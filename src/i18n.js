import i18n from 'i18next';
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
    .use(initReactI18next)
    .use(LanguageDetector)
    .init({
        // we init with resources
        resources: {
            en: {
                translations: {
                    "Continue":"Continue",
                    "Logout":"Logout",
                    "GuestDashboardText_1":"Three simple, yet difficult steps await you",
                    "Dashboard":"Dashboard",
                    "Hack yourself": "Hack yourself",
                    "Support NGO": "Support NGO",
                    "Our Collective Text": `Composed of digital consumers and web developers, we are joining forces to create free tools as alternatives to private software and encourage the emergence of new collaborative platforms.`,
                    "Our solution": "Our solution",
                    "Our Solution Text": `The Dev4Good platform aims to put web players (developer, data analyst, UX / UI designer) in touch with the leaders of collective projects.
                    <br>
                    Our action aims to set up digital tools with the objective of:
                    <br>
                    - to assist in the sustainable development of local social and environmental actions;<br/>
                    - foster the cooperation of local actors;<br/>
                    - promote the exchange of knowledge and skills and the use of free software.<br/>
                    Anyone with a project in line with these values is invited to propose it in order to receive concrete support from volunteers from our organization.`,
                    "Discover": "Discover",
                    "Sign Up": "Sign Up",
                    "Login": "Login"
                }
            },
            fr: {
                translations: {
                    "Continue":"Continuer",
                    "Logout":"Se déconnecter",
                    "GuestDashboardText_1":"Clair comme l'eau, le long du chemin vous attend, mais la route est libre",
                    "Dashboard":"Tableau de board",
                    "Hack yourself": "Hackez-vous",
                    "Support NGO": "Soutien des ONG",
                    "Build Network": "Construisez votre réseau",
                    "The Collective": "Notre collectif",
                    "Our Collective Text": `Composé de consommateurs digitaux et de développeurs web, nous nous unissons afin de créer ensemble des outils libres comme alternatives aux logiciels privés et favoriser l’émergence de nouvelles plateformes collaboratives.`,
                    "Our solution": "Notre solution",
                    "Our Solution Text": `La platforme de Dev4Good vise a mettre en relation les acteurs du web (developpeur, data analyst, designer UX/UI) et les porteurs de projets collectifs.
                    <br/>
                    Notre action vise a mettre en place des outils digitaux avec pour objectif:
                    <br/>
                    - d’aider au developpement durable d’actions locales sociales et environementales ;<br>
                    - favoriser la coopération des acteurs locaux ;<br>
                    - promouvoir l’echange de savoir et de competences et l’utilisation des logiciel libres.<br>
                    Toute personne ayant un projet en accord avec ces valeurs est invite a le proposer afin de recevoir un support concret des benevolles de notre organisation.`,
                    "Discover": "Découvrir",
                    "Sign Up": "S'inscrire",
                    "Login": "S'identifier"
                }
            }
        },
        fallbackLng: 'en',
        debug: true,

        // have a common namespace used around the full app
        ns: ['translations'],
        defaultNS: 'translations',

        keySeparator: false, // we use content as keys

        interpolation: {
            escapeValue: false, // not needed for react!!
            formatSeparator: ','
        },

        react: {
            wait: true
        }
    });

export default i18n;