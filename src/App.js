import React, { Component } from "react";

import { Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
//import Private from "./pages/Private";
import UserPortal from "./pages/UserPortal";
import UserProfileEdit from "./pages/UserProfileEdit";
import InitiatorDashboard from "./pages/InitiatorDashboard";
import InitiatorAddProject from "./pages/InitiatorAddProject";
import InitiatorEditProject from "./pages/InitiatorEditProject";
import ParticipantDashboard from "./pages/ParticipantDashboard";
import ParticipantSeekProject from "./pages/ParticipantSeekProject";
import ParticipantEditProject from "./pages/ParticipantEditProject";
import SeekUsers from "./pages/SeekUsers";
import SeeUserDetail from "./pages/SeeUserDetail";

import AnonRoute from "./components/AnonRoute";
import PrivateRoute from "./components/PrivateRoute";

import loadable from '@loadable/component'
const Landing = loadable(() => import(/* webpackChunkName: "landing" */'./pages/Landing'))


class App extends Component {
  render() {
    return (
      <div className="app">
        <Navbar />

        <Switch>
          <AnonRoute exact path="/" component={Landing} />
          <AnonRoute exact path="/home" component={Home} />
          <AnonRoute exact path="/signup" component={Signup} />
          <AnonRoute exact path="/login" component={Login} />

          

          <PrivateRoute exact path="/user-portal" component={UserPortal} />
          <PrivateRoute exact path="/user-profile-edit" component={UserProfileEdit} />
          <PrivateRoute exact path="/initiator-dashboard" component={InitiatorDashboard} />
          <PrivateRoute exact path="/initiator-add-project" component={InitiatorAddProject} />
          <PrivateRoute exact path="/initiator-edit-project/:id" component={InitiatorEditProject} />
          <PrivateRoute exact path="/participant-dashboard" component={ParticipantDashboard} />
          <PrivateRoute exact path="/participant-seek-project" component={ParticipantSeekProject} />
          <PrivateRoute exact path="/participant-edit-project/:id" component={ParticipantEditProject} />
          <PrivateRoute exact path="/seek-users" component={SeekUsers} />
          <PrivateRoute exact path="/see-user-detail/:id" component={SeeUserDetail} />


          
        </Switch>
      </div>
    );
  }
}

export default App;
