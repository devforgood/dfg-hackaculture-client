import React, { Component } from "react";
import { withAuth } from "./../lib/Auth";
import { Link } from "react-router-dom";
import userService from "./../lib/user-service";
import ParticipantDashboardCard from "./../components/ParticipantDashboardCard"
import { Page } from '../styled/layouts'

class ParticipantDashboard extends Component {

    state = {
        appliedOnProject: [],
        acceptedOnProject: []
        //[aaaaadddddazzzzz,qqqqqqqqqq]
        //[5e623b502540e5986089d876,5e623c948776f098c17108bd]

    }

    componentDidMount() {
        const { _id } = this.props.user;
        userService.getOne(_id)
            .then((user) => {
                // console.log('user', user.iniatorOnProject)
                this.setState({
                    appliedOnProject: user.appliedOnProject,
                    acceptedOnProject: user.acceptedOnProject
                })
            })
            .catch((error) => console.log('error', error))
    }

    render() {
        return (

            <Page >

                <h1 className="title is-2 level-item" >Participant</h1>

                <div className=" field level-item">
                    <div className="control">
                        <Link className="button is-black" to="/participant-seek-project">
                            <h4>Seek for Project</h4>
                        </Link>
                    </div>
                </div>

                <h3 className="subtitle is-3" >Applied</h3>
                {
                    this.state.appliedOnProject.map((project) => {

                        return <ParticipantDashboardCard key={project._id} {...project} />
                    })
                }

                <h3 className="subtitle is-3" >Accepted</h3>

                {
                    this.state.acceptedOnProject.map((project) => {

                        return <ParticipantDashboardCard key={project._id} {...project} />
                    })
                }

            </Page>
        )
    }
}

export default withAuth(ParticipantDashboard);