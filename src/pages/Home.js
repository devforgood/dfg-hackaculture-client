import React from 'react'
import { Link } from "react-router-dom";
import styled from '@emotion/styled'
import { withTranslation, Trans } from 'react-i18next';
import { withAuth } from "./../lib/Auth";
import LogoImage from '../assets/img/discover.jpg'
import {ContinueButton} from '../styled/buttons'
const Title = styled.h1`
font-size: 25px;
font-weight: 900;
color: #30894e;
`

const Logo = styled.div`
  max-width:600px;
  margin: 0 auto;
  margin-top:50px;
`


class Home extends React.Component {
  handleContinue(){
    console.log({
      "this.props.isLoggedIn": this.props.isLoggedIn
    })
    if(this.props.isLoggedIn){
      this.props.history.push("/user-portal")
    }else{
      this.props.history.push("/signup")
    }
  }
  render() {
    const {  t } = this.props;
    return (
      <Logo >
        <Title>{t("GuestDashboardText_1")}</Title>
        <img src={LogoImage} />
        <ContinueButton onClick={()=>this.handleContinue()}>
            {t('Continue')}
        </ContinueButton>
      </Logo>
    )
  }
}
export default withAuth(withTranslation('translations')(Home));
