import React, { Component } from "react";
import { withAuth } from "./../lib/Auth";
import styled from '@emotion/styled'

const FormBox = styled.div`
  margin-top:50px;
  @media (min-width: 992px) {
    max-width:900px;
    margin:0 auto;
    margin-top:50px;
  }
`



class Login extends Component {
  state = { email: "", password: "" };

  handleFormSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;

    this.props.login(email, password);
  };

  componentDidUpdate(){
    if(this.props.isLoggedIn){
      this.props.history.push("/user-portal")
    }
  }

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handlePasswordReset(){
    if(!this.state.email){
      return alert("Email needed")
    }
    this.props.resetPwd(this.state.email).then(()=>{
      this.setState({
        email:"",
        password:""
      })
      alert("If your email address exists in our database, you will receive an email :)")
    })
  }

  render() {
    const { email, password } = this.state;

    return (
      <FormBox className="box notification field level">
        <h1 className="title is-2 level-item">Login</h1>

        <form onSubmit={this.handleFormSubmit}>
          <label className="label">Email:</label>
          <input className="input"
            type="text"
            name="email"
            value={email}
            onChange={this.handleChange}
          />

          <label className="label">Password:</label>
          <input className="input"
            type="password"
            name="password"
            value={password}
            onChange={this.handleChange}
          />
          <div className="level-item button-padding">
            <input className="button is-black " type="submit" value="Login" />
          </div>

          <p className="level-item">Do not remember your password?&nbsp; <a onClick={()=>this.handlePasswordReset()}> Reset</a></p>
          

        </form>
      </FormBox>
    );
  }
}

export default withAuth(Login);
