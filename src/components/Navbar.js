import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withAuth } from "./../lib/Auth";
import styled from '@emotion/styled'
import { withTranslation, Trans } from 'react-i18next';


const Logo = styled.img`
  max-width:250px;
  max-height:initial!important;
`

const Nav = styled.nav`
    min-height:5.25rem;
`

class Navbar extends Component {
  async changeLanguage(lang){
    await this.props.i18n.changeLanguage(lang)
  }
  componentDidMount(){
    document.addEventListener('DOMContentLoaded', () => {

        // Get all "navbar-burger" elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
      
        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {
      
          // Add a click event on each of them
          $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {
      
              // Get the target from the "data-target" attribute
              const target = el.dataset.target;
              const $target = document.getElementById(target);
      
              // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
              el.classList.toggle('is-active');
              $target.classList.toggle('is-active');
      
            });
          });
        }
      
      }); 
  }
  render() {
    const { user, logout, isLoggedIn, t } = this.props;

    return (
        <Nav className="navbar">
            <div className="navbar-brand">
                <Link className="navbar-item" to={"/"} id="home-btn">
                    <Logo src="https://res.cloudinary.com/misitioba/image/upload/v1585331187/dfg/dfg_logo_text.svg"/>
                </Link>

                <a role="button" className="navbar-burger burger" aria-label="menu" 
                aria-expanded="false" data-target="navBarMenu">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navBarMenu" className="navbar-menu ">
                
                <div className="navbar-end">
                {isLoggedIn   
                    ? (
                    <>
                    <Link className="navbar-item" to={"/user-portal"}>
                        {t('Dashboard')}
                    </Link>
                    <Link className="navbar-item" to={"/user-profile-edit"}>
                        <h4>{user.firstName}</h4>
                    </Link>
                    <a className="navbar-item" onClick={logout}>
                      {t('Logout')}
                    </a>
                    </>
                    ):(
                        <>
                        <Link className="navbar-item" to="/login">
                        {t('Login')}
                        </Link>
                        <Link className="navbar-item" to="/signup"> 
                        {t('Sign Up')}
                        </Link>
                        </>
                    )}

                        <a className="navbar-item" onClick={()=>this.changeLanguage.apply(this,['en'])}>
                        EN
                        </a>
                        <a className="navbar-item" onClick={()=>this.changeLanguage.apply(this,['fr'])}> 
                        FR
                        </a>


                </div>    
            </div>
        
        </Nav>
    );
  }
}

export default withTranslation('translations')(withAuth(Navbar));
