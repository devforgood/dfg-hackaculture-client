import { Link } from "react-router-dom";
import styled from '@emotion/styled'

function BaseButton(type) {
  let style = `
  margin: 0 auto;
  outline: 2px solid #34b85f;
  padding: 20px;
  text-align:center;
  font-size: 18px;
  font-weight: 900;
  display:block;
  &:hover{
    color: #34b85f;
  }
`;
  if (['a', 'button'].includes(type)) {
    return styled[type](style)
  }
  return styled(type)(style)
}


export const OutlineButton = BaseButton(Link)

export const DiscoverButton = styled(OutlineButton)`
  margin-top:50px;
  width:250px;
`

export const ContinueButton = (styled(BaseButton('a')))(`
margin-top:50px;
width:250px;
`)