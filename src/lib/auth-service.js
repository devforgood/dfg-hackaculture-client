import axios from "axios";

class Auth {
  constructor() {
    this.auth = axios.create({
      // baseURL: "http://localhost:5000",
      baseURL: process.env.REACT_APP_API_URL,
      withCredentials: true                         // Axios syntaxt
    });
  }

  resetPwd({email}){
    return this.auth
      .post("/auth/password_reset", { email })
      .then(({ data }) => data);
  }

  signup({ firstName, lastName, email, password, location, skills, preferedProject }) {
    return this.auth
      .post("/auth/signup", { firstName, lastName, email, password, location, skills, preferedProject })
      .then(({ data }) => data);
    // .then((response) => response.data);
  }

  login({ email, password }) {
    return this.auth
      .post("/auth/login", { email, password })
      .then(({ data }) => data);
    // .then((response) => response.data);
  }

  logout() {
    return this.auth.post("/auth/logout", {}).then(({ data }) => data);
    // return this.auth.post("/auth/logout", {}).then((response) => response.data);
  }

  me() {
    return this.auth.get("/auth/me").then(({ data }) => data);
    // return this.auth.get("/auth/me").then((response) => response.data);
  }
}

const authService = new Auth();
// `authService` is the object with the above axios request methods

export default authService;
