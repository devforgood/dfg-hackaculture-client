const express = require('express')
const app = express()
const execSh = require('exec-sh')
const PORT = process.env.PORT || 3000;
const path = require('path')
app.use(express.static('dist'));
app.get('*', (req, res) => res.sendFile(path.resolve('dist', 'index.html')));
app.listen(PORT, async() => {
    console.log(`Listening on ${PORT}`)

    var prerenderHook = "https://webhooks.misitioba.com/hook/dfg-web-prerender";

    const https = require('https');

    https.get(prerenderHook, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            console.log(data.toString("utf-8"));
        });
    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });

});